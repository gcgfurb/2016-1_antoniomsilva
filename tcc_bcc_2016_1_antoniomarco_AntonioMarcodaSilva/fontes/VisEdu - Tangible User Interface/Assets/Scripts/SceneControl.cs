﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class SceneControl {

    public static Vector3 Bezier2(Vector3 Start, Vector3 Control, Vector3 End, float t)
    {
        return (((1-t)*(1-t)) * Start) + (2 * t* (1 - t) * Control) + ((t* t) * End);
    }

    public static GameObject getObjectFactory()
    {
        return GameObject.Find("ObjectFactory");
    }
    public static GameObject getFactoryPlate()
    {
        return GameObject.Find("FactoryPlate");
    }
    public static bool isChildFactoryPlate(GameObject go)
    {
        return (go.transform.parent == getFactoryPlate().transform);
    }
    public static bool isChildObjectFactory(GameObject go)
    {
        return (go.transform.parent == getObjectFactory().transform);
    }
    public static Vector3 PositionRelativeBottom(Vector3 obj, Vector3 position)
    {
        return new Vector3(position.x, position.y + (obj.y/2), position.z);

    }
    public static Vector3 ScaleToFit(GameObject obj, Vector3 size)
    {
        Vector3 objSize = obj.GetComponent<Renderer>().bounds.size;
        Vector3 ret = new Vector3(objSize.x, objSize.y, objSize.z);
        float vy = objSize.y;
        if (ret.y > size.y) {
            vy = size.y / objSize.y;
            ret.x *= vy;
            ret.y *= vy;
            ret.z *= vy;
        }
        if(ret.x > size.x ){
            vy = size.x / objSize.x;
            ret.x *= vy;
            ret.y *= vy;
            ret.z *= vy;
        }

        if (ret.z > size.z) {
            vy = size.z / objSize.z;
            ret.x *= vy;
            ret.y *= vy;
            ret.z *= vy;
        }
        return ret;
    }

    public static void ButtonCoolDown(ref GameObject[] buttons, ref float cooldownTimer, ref float buttonCooldown)
    {
        if (cooldownTimer >= 0 && cooldownTimer < buttonCooldown)
        {
            Color color = Color.Lerp(new Color(1, 0, 0, 0.5f), new Color(0, 0, 1, 0.5f), Mathf.Clamp01(cooldownTimer / buttonCooldown));
            foreach(GameObject button in buttons)
                button.GetComponent<Renderer>().material.color = color;
            cooldownTimer += Time.fixedDeltaTime;
        }
        else
        {
            foreach (GameObject button in buttons)
                button.GetComponent<Renderer>().material.color = new Color(0, 0, 1, 0.5f);
        }
    }

    public static bool isZeroVector(Vector3 vec)
    {
        return vec.x == 0 && vec.y == 0 && vec.z == 0; 
    }

    
}
