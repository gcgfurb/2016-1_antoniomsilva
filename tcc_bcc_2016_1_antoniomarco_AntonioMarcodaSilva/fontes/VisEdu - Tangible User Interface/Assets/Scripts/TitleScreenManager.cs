﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TitleScreenManager : MonoBehaviour {

    


    public void openURL(string questionarioUrl) {
        Application.OpenURL(questionarioUrl);
    }
    public void loadScene(int scene) {
        SceneManager.LoadSceneAsync(scene);
    }
    public void switchActive(GameObject panel) {
        panel.SetActive(!panel.activeSelf);
    }

}
