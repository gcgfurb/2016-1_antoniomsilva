﻿using UnityEngine;
using System.Collections;

public class TrashManager : MonoBehaviour {

    private float timer = 0;
    public float duracao = 2f;
    public float movingDuration = 2f;

    private Animator anim;
    private GameObject objetoRemovido = null;
    private Vector3 posicaoObjeto;
    private float movingTimer = 0f;
    private Vector3 escalaObjeto;

    void Start () {

        anim = transform.FindChild("TrashCanTampa").GetComponent<Animator>();
	}

	void Update () {
	    if(objetoRemovido != null) {
            if (movingTimer < movingDuration) {
                movingTimer += Time.deltaTime / movingDuration;
                objetoRemovido.transform.position = SceneControl.Bezier2(transform.position, transform.position + (transform.up * 10), posicaoObjeto, movingTimer*0.8f);
                objetoRemovido.transform.localScale = Vector3.Lerp(escalaObjeto, escalaObjeto*0.2f, movingTimer);
            }
            else {
                Destroy(objetoRemovido);
            }
        }
	}
    void OnTriggerEnter(Collider colliderInfo) {
        if (colliderInfo.gameObject.tag.Equals("Guides") || colliderInfo.gameObject.tag.Equals("GuidesConnector"))
            return;
        if (colliderInfo.gameObject.transform.parent != SceneControl.getFactoryPlate() 
            && colliderInfo.gameObject.transform.parent != SceneControl.getObjectFactory()) {
            timer = 0;
            anim.SetBool("opening", true);
        }
    }
    void OnTriggerStay(Collider colliderInfo) {
        if (colliderInfo.gameObject.tag.Equals("Guides") || colliderInfo.gameObject.tag.Equals("GuidesConnector"))
            return;
        if (colliderInfo.gameObject.transform.parent != SceneControl.getFactoryPlate() 
            && colliderInfo.gameObject.transform.parent != SceneControl.getObjectFactory()) {
            if (timer >= duracao || objetoRemovido != null) {
                objetoRemovido = colliderInfo.gameObject;
                objetoRemovido.transform.SetParent(null);
                posicaoObjeto = objetoRemovido.transform.position;
                escalaObjeto = objetoRemovido.transform.localScale;
            } else {
                timer += Time.fixedDeltaTime;
            }
        }
    }
    void OnTriggerExit(Collider colliderInfo) {
        if (colliderInfo.gameObject.transform.parent != SceneControl.getFactoryPlate() 
            && colliderInfo.gameObject.transform.parent != SceneControl.getObjectFactory()) {
            anim.SetBool("opening", false);
        }
    }
}
