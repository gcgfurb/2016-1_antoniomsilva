﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class ObjectFactory : MonoBehaviour,IVirtualButtonEventHandler {

    public GameObject[] objects;
    public GameObject ObjetoPosicao;

    public GameObject[] buttons;
    
    public float buttonCooldown = 1;

    private float buttonTimer = 0;
    private float cooldownTimer = 0;
    private GameObject ObjetoSelecionado;
    private int valorSelecionado = 0;
	// Use this for initialization
	void Start () {
        cooldownTimer = buttonCooldown;
		VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
		for (int i = 0; i < vbs.Length; ++i) {
			// Register with the virtual buttons TrackableBehaviour
			vbs[i].RegisterEventHandler(this);
		}
        if(objects.Length > 0)
        {

            this.setObject(valorSelecionado);
        }
        else
        {
            Debug.Log("Lista de Objetos está vazio!");
        }
	}

    private void setObject(int value)
    {
        if (ObjetoSelecionado != null)
            Destroy(ObjetoSelecionado);
        ObjetoSelecionado = Instantiate(objects[value], new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
        if (ObjetoSelecionado.GetComponent<Collider>() == null)
        {
            BoxCollider bc = ObjetoSelecionado.AddComponent<BoxCollider>() as BoxCollider;
            bc.size = new Vector3(1, 1, 1);
            bc.center = new Vector3(0, 0, 0);
        }
        if (ObjetoSelecionado.GetComponent<Modifiers>() == null)
            ObjetoSelecionado.AddComponent<Modifiers>();
        ObjetoSelecionado.transform.SetParent(ObjetoPosicao.transform);
        ObjetoSelecionado.transform.localScale = SceneControl.ScaleToFit(ObjetoSelecionado, new Vector3(0.5f,0.5f,0.5f));
        ObjetoSelecionado.transform.position = SceneControl.PositionRelativeBottom(ObjetoSelecionado.GetComponent<Renderer>().bounds.size, ObjetoPosicao.transform.position);
    }
    void Update()
    {
        SceneControl.ButtonCoolDown(ref buttons, ref cooldownTimer, ref buttonCooldown);
    }


	public void OnButtonPressed (VirtualButtonAbstractBehaviour vb){

        if (cooldownTimer >= buttonCooldown)
        {
            switch (vb.VirtualButtonName)
            {
                case "Left":
                    buttonTimer += Time.fixedDeltaTime;
                    if (valorSelecionado <= 0)
                        valorSelecionado = objects.Length;
                    setObject(--valorSelecionado);
                    buttons[0].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                    cooldownTimer = 0;
                    break;
                case "Right":
                    buttonTimer += Time.fixedDeltaTime;
                    if (valorSelecionado >= (objects.Length-1))
                        valorSelecionado = -1;
                    setObject(++valorSelecionado);
                    buttons[1].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                    cooldownTimer = 0;
                    break;
            }
        }
	}
	public void OnButtonReleased(VirtualButtonAbstractBehaviour vb) {
	}
}
