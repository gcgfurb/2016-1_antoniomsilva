﻿using UnityEngine;
using System.Collections;

public class SceneMarkerManager : MonoBehaviour {
    
    public float movingDuration = 1f;
    public float duracao = 2f;
    private GameObject objetoMovido;

    private float movingTimer = 0f;
    private Vector3 posicaoObjeto;
    private float timer = 0;
    private Quaternion rotacaoObjeto;
    // Use this for initialization
    void Start () {
        GetComponent<Renderer>().material.color = Color.red;
    }
	
	// Update is called once per frame
	void Update () {
        if (objetoMovido != null) {
            if (movingTimer < movingDuration) {
                movingTimer += Time.deltaTime / movingDuration;
                objetoMovido.transform.position = SceneControl.Bezier2(transform.position, transform.position + (transform.up * 10), posicaoObjeto, movingTimer);
                objetoMovido.transform.position = Vector3.Lerp(posicaoObjeto, SceneControl.PositionRelativeBottom(objetoMovido.GetComponent<Renderer>().bounds.size,transform.position), movingTimer);
                objetoMovido.transform.rotation = Quaternion.Lerp(rotacaoObjeto, transform.rotation, movingTimer);
                GetComponent<Renderer>().material.color = Color.green;
                StartCoroutine(waitSeconds(1));
            } else {
                objetoMovido.transform.SetParent(SceneControl.getObjectFactory().transform);
                if (objetoMovido.GetComponent<Modifiers>() == null) {
                    objetoMovido.AddComponent<Modifiers>();
                    objetoMovido.GetComponent<Modifiers>().setValues();
                } else {
                    objetoMovido.GetComponent<Modifiers>().setValues();
                }
                objetoMovido = null;
                GetComponent<Renderer>().material.color = Color.red;
                movingTimer = 0;
            }
        }
    }

    private IEnumerator waitSeconds(int sec)
    {
        yield return new WaitForSeconds(sec);
    }


    void OnTriggerEnter(Collider colliderInfo) {
        timer = 0;
    }

    void OnTriggerStay(Collider colliderInfo) {
        if (!SceneControl.isChildFactoryPlate(colliderInfo.gameObject) && !SceneControl.isChildObjectFactory(colliderInfo.gameObject) && colliderInfo.gameObject.tag!="GuidesConnector" && colliderInfo.gameObject.tag!="Guides") {
            GetComponent<Renderer>().material.color = Color.yellow;
            if (timer >= duracao || objetoMovido != null) {
                objetoMovido = colliderInfo.gameObject;
                objetoMovido.transform.SetParent(null);
                posicaoObjeto = objetoMovido.transform.position;
                rotacaoObjeto = objetoMovido.transform.rotation;
            } else {
                timer += Time.fixedDeltaTime;
            }
        }
    }
    void OnTriggerExit(Collider colliderInfo) {
        GetComponent<Renderer>().material.color = Color.red;
        timer = 0;
    }

}
