﻿using UnityEngine;
using System.Collections;
using Vuforia;
using UnityEngine.UI;
using System;

public class SettingManager : MonoBehaviour, ITrackableEventHandler, IVirtualButtonEventHandler
{

    public GameObject canvasObject;
    public Text titleText;
    public Text descriptionText;
    public float distancia = 15;
    public float secondsToHold = 2;
    public float buttonCooldown = 1;
    public GameObject[] buttons;

    private float buttonTimer = 0;
    private GameObject objetoSelecionado = null;
    private bool activeCanvas = false;
    private TrackableBehaviour mTrackableBehaviour;
    private Vector3 posicaoLineStart;
    private Vector3 posicaoLineEnd;
    private LineRenderer line;
    private Ray ray;
    private float timer = 0;
    private int maskLayer = 1 << 0;
    private int activeModifier = 0;
    private Modifiers modObject;
    private Canvas canvas;
    private int maxModifier = 8;

    private float cooldownTimer = 0;

    void Start()
    {
        cooldownTimer = buttonCooldown;
        canvas = canvasObject.GetComponent<Canvas>() as Canvas;
        line = GetComponent<LineRenderer>();
        setLinePosition();
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
        for (int i = 0; i < vbs.Length; ++i)
        {
            // Register with the virtual buttons TrackableBehaviour
            vbs[i].RegisterEventHandler(this);
        }
        line = gameObject.GetComponent<LineRenderer>();
        line.SetColors(Color.red, Color.red);
        line.SetPosition(1, new Vector3(0, distancia, 0));
        line.SetWidth(0.4f, 0.4f);

        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        ray = new Ray(posicaoLineStart, posicaoLineEnd);

    }

    private void updateTitle()
    {
        if (!canvas.enabled || objetoSelecionado == null)
            return;
        if (objetoSelecionado.GetComponent<Modifiers>() == null)
            modObject = objetoSelecionado.AddComponent<Modifiers>() as Modifiers;
        switch (activeModifier)
        {
            case 0:
                titleText.text = "Posição X:";
                break;
            case 1:
                titleText.text = "Posição Y:";
                break;
            case 2:
                titleText.text = "Posição Z:";
                break;
            case 3:
                titleText.text = "Rotação X:";
                break;
            case 4:
                titleText.text = "Rotação Y:";
                break;
            case 5:
                titleText.text = "Rotação Z:";
                break;
            case 6:
                titleText.text = "Escala X:";
                break;
            case 7:
                titleText.text = "Escala Y:";
                break;
            case 8:
                titleText.text = "Escala Z:";
                break;

        }
    }

    public void UpdateDescription()
    {
        if (!canvas.enabled || objetoSelecionado == null)
            return;
        if (objetoSelecionado.GetComponent<Modifiers>() == null)
            modObject = objetoSelecionado.AddComponent<Modifiers>() as Modifiers;
        switch (activeModifier) {
            case 0:
                descriptionText.text = modObject.getOriginalPosition().x + " + (" + modObject.getModPosition().x + ")";
                break;
            case 1:
                descriptionText.text = modObject.getOriginalPosition().y + " + (" + modObject.getModPosition().y + ")";
                break;
            case 2:
                descriptionText.text = modObject.getOriginalPosition().z + " + (" + modObject.getModPosition().z + ")";
                break;
            case 3:
                descriptionText.text = modObject.getOriginalRotation().x + " + (" + modObject.getModRotation().x + ")";
                break;
            case 4:
                descriptionText.text = modObject.getOriginalRotation().y + " + (" + modObject.getModRotation().y + ")";
                break;
            case 5:
                descriptionText.text = modObject.getOriginalRotation().z + " + (" + modObject.getModRotation().z + ")";
                break;
            case 6:
                descriptionText.text = modObject.getOriginalScale().x + " + (" + modObject.getModScale().x + ")";
                break;
            case 7:
                descriptionText.text = modObject.getOriginalScale().y + " + (" + modObject.getModScale().y + ")";
                break;
            case 8:
                descriptionText.text = modObject.getOriginalScale().z + " + (" + modObject.getModScale().z + ")";
                break;
        }
    }

    void Update()
    {
        activateCanvas();
        getObjectByLine();
        setLinePosition();
        UpdateDescription();
        SceneControl.ButtonCoolDown(ref buttons, ref cooldownTimer, ref buttonCooldown);
    }



    private void setLinePosition() {
        if (activeCanvas && !line.enabled && objetoSelecionado != null) {
            line.enabled = true;
        }
        if (line.enabled) {
            if (!activeCanvas)
                line.enabled = false;
            else if (objetoSelecionado != null && !objetoSelecionado.GetComponent<Renderer>().isVisible)
                line.enabled = false;
            else {
                posicaoLineStart = transform.position + (transform.forward * 1.8f);
                posicaoLineEnd = transform.position + (transform.forward * distancia);
                line.SetPosition(0, posicaoLineStart);
                if (objetoSelecionado != null)
                    line.SetPosition(1, objetoSelecionado.transform.position);
                else
                    line.SetPosition(1, posicaoLineEnd);
            }
        }
    }

    public void activateCanvas()
    {
        if ((!activeCanvas || objetoSelecionado == null) && canvas.enabled)
            canvas.enabled = false;
        else if (activeCanvas && !canvas.enabled && objetoSelecionado != null)
        {
            canvas.enabled = true;
            updateTitle();
        }


    }

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        if (canvas.enabled) {
            if (cooldownTimer >= buttonCooldown) {
                switch (vb.VirtualButtonName) {
                    case "Up":
                        buttonTimer += Time.fixedDeltaTime;
                        if (activeModifier >= maxModifier)
                            activeModifier = 0;
                        else
                            activeModifier++;
                        buttons[0].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                        cooldownTimer = 0;
                        updateTitle();
                        break;
                    case "Down":
                        buttonTimer += Time.fixedDeltaTime;
                        if (activeModifier <= 0)
                            activeModifier = maxModifier;
                        else
                            activeModifier--;
                        buttons[1].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                        cooldownTimer = 0;
                        updateTitle();
                        break;
                    case "Cancel":
                        buttonTimer += Time.fixedDeltaTime;
                        activeModifier = 0;
                        buttons[2].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                        updateTitle();
                        objetoSelecionado = null;
                        break;
                }
            }
        }
    }

    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
            activeCanvas = true;
        } else {
            activeCanvas = false;
        }

    }

    private void getObjectByLine() {
        ray = new Ray(posicaoLineStart, transform.forward);
        RaycastHit hitInfo;
        if (objetoSelecionado == null) {
            if (Physics.Raycast(ray, out hitInfo, distancia + 2, maskLayer)) {
                if (SceneControl.isChildObjectFactory(hitInfo.collider.gameObject)) {
                    if (hitInfo.distance > distancia) {
                        timer = 0;
                        line.SetColors(Color.red, Color.red);
                    }
                    else {
                        timer += Time.deltaTime;
                        line.SetColors(Color.yellow, Color.yellow);
                    }
                    if (timer >= secondsToHold) {
                        line.SetColors(Color.green, Color.green);
                        objetoSelecionado = hitInfo.collider.gameObject;
                        modObject = objetoSelecionado.GetComponent<Modifiers>() as Modifiers;
                        timer = 0;
                        updateTitle();
                    }
                }
            } else {
                line.SetColors(Color.red, Color.red);
            }
        } else {
            line.SetColors(Color.green, Color.green);
        }
    }
    public bool getActiveCanvas() {
        return activeCanvas;
    }

    public GameObject getSelectedObject() {
        if (objetoSelecionado != null)
            return objetoSelecionado;
        else
            return null;
    }

    public int getActiveMod()
    {
        return activeModifier;
    }

    public float getActualValue()
    {
        switch (activeModifier)
        {
            case 0:
                return modObject.getModdedPosition().x;
            case 1:
                return modObject.getModdedPosition().y;
            case 2:
                return modObject.getModdedPosition().z;
            case 3:
                return modObject.getModdedRotation().x;
            case 4:
                return modObject.getModdedRotation().y;
            case 5:
                return modObject.getModdedRotation().z;
            case 6:
                return modObject.getModdedScale().x;
            case 7:
                return modObject.getModdedScale().y;
            case 8:
                return modObject.getModdedScale().z;
        }
        return 0;
    }

    public float getModValue()
    {
        switch (activeModifier)
        {
            case 0:
                return modObject.getModPosition().x;
            case 1:
                return modObject.getModPosition().y;
            case 2:
                return modObject.getModPosition().z;
            case 3:
                return modObject.getModRotation().x;
            case 4:
                return modObject.getModRotation().y;
            case 5:
                return modObject.getModRotation().z;
            case 6:
                return modObject.getModScale().x;
            case 7:
                return modObject.getModScale().y;
            case 8:
                return modObject.getModScale().z;
        }
        return 0;
    }

    public void setActualValue(float value)
    {
        switch (activeModifier)
        {
            case 0:
                modObject.setModPositionX(value);
                break;
            case 1:
                modObject.setModPositionY(value);
                break;
            case 2:
                modObject.setModPositionZ(value);
                break;
            case 3:
                modObject.setModRotationX(value);
                break;
            case 4:
                modObject.setModRotationY(value);
                break;
            case 5:
                modObject.setModRotationZ(value);
                break;
            case 6:
                modObject.setModScaleX(value);
                break;
            case 7:
                modObject.setModScaleY(value);
                break;
            case 8:
                modObject.setModScaleZ(value);
                break;
        }
    }
    public void setMotorValue(float value)
    {
        switch (activeModifier)
        {
            case 0:
                modObject.setMotorPositionX(value);
                break;
            case 1:
                modObject.setMotorPositionY(value);
                break;
            case 2:
                modObject.setMotorPositionZ(value);
                break;
            case 3:
                modObject.setMotorRotationX(value);
                break;
            case 4:
                modObject.setMotorRotationY(value);
                break;
            case 5:
                modObject.setMotorRotationZ(value);
                break;
            case 6:
                modObject.setMotorScaleX(value);
                break;
            case 7:
                modObject.setMotorScaleY(value);
                break;
            case 8:
                modObject.setMotorScaleZ(value);
                break;
        }

    }
    public float getMotorValue()
    {
        switch (activeModifier)
        {
            case 0:
                return modObject.getMotorPosition().x;
            case 1:
                return modObject.getMotorPosition().y;
            case 2:
                return modObject.getMotorPosition().z;
            case 3:
                return modObject.getMotorRotation().x;
            case 4:
                return modObject.getMotorRotation().y;
            case 5:
                return modObject.getMotorRotation().z;
            case 6:
                return modObject.getMotorScale().x;
            case 7:
                return modObject.getMotorScale().y;
            case 8:
                return modObject.getMotorScale().z;
        }
        return 0;
    }
}
