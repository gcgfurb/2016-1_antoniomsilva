﻿using UnityEngine;
using System.Collections;

public class Modifiers : MonoBehaviour {

    private Vector3 objectPosition;
    private Vector3 posModifier;

    private Vector3 objectScale;
    private Vector3 scaleModifier;

    private Vector3 objectRotation;
    private Vector3 rotationModifier;
    private Vector3 motorPosition;
    private Vector3 motorScale;
    private Vector3 motorRotation;


    // Use this for initialization
    void Start () {
        
        objectPosition = transform.localPosition;
        objectRotation = transform.localEulerAngles;
        objectScale = transform.localScale;

        posModifier = new Vector3(0, 0, 0);
        scaleModifier = new Vector3(0, 0, 0);
        rotationModifier = new Vector3(0, 0, 0);

        motorPosition = new Vector3(0, 0, 0);
        motorScale = new Vector3(0, 0, 0);
        motorRotation = new Vector3(0, 0, 0);
    }
	
	// Update is called once per frame
	void Update () {
        //getObjectValues();
        updateMotor();
    }

    public Vector3 getOriginalPosition() { return objectPosition; }
    public Vector3 getOriginalScale() { return objectScale; }
    public Vector3 getOriginalRotation() { return objectRotation; }

    public Vector3 getModdedPosition() { return objectPosition+posModifier; }
    public Vector3 getModdedScale() { return objectScale+scaleModifier; }
    public Vector3 getModdedRotation() { return objectRotation+rotationModifier; }

    public Vector3 getModPosition() { return posModifier; }
    public Vector3 getModScale() { return scaleModifier; }
    public Vector3 getModRotation() { return rotationModifier; }

    public void setModPosition(Vector3 value) { posModifier = value; updateObject(); }
    public void setModPositionX(float value) { posModifier.x = value; updateObject(); }
    public void setModPositionY(float value) { posModifier.y = value; updateObject(); }
    public void setModPositionZ(float value) { posModifier.z = value; updateObject(); }

    public void setMotorPositionX(float value){ motorPosition.x = value; }
    public void setMotorPositionY(float value) { motorPosition.y = value; }
    public void setMotorPositionZ(float value) { motorPosition.z = value; }

    public void setModScale(Vector3 value) { scaleModifier = value; updateObject(); }
    public void setModScaleX(float value) { scaleModifier.x = value; updateObject(); }
    public void setModScaleY(float value) { scaleModifier.y = value; updateObject(); }
    public void setModScaleZ(float value) { scaleModifier.z = value; updateObject(); }

    public void setMotorScaleX(float value) { motorScale.x = value; }
    public void setMotorScaleY(float value) { motorScale.y = value; }
    public void setMotorScaleZ(float value) { motorScale.z = value; }

    public void setModRotation(Vector3 value) { rotationModifier = value; updateObject(); }
    public void setModRotationX(float value) { rotationModifier.x = value; updateObject(); }
    public void setModRotationY(float value) { rotationModifier.y = value; updateObject(); }
    public void setModRotationZ(float value) { rotationModifier.z = value; updateObject(); }

    public void setMotorRotationX(float value) { motorRotation.x = value; }
    public void setMotorRotationY(float value) { motorRotation.y = value; }
    public void setMotorRotationZ(float value) { motorRotation.z = value; }

    public Vector3 getMotorPosition() { return motorPosition; }

    public Vector3 getMotorScale() { return motorScale; }
    public Vector3 getMotorRotation() { return motorRotation; }

    private void updateMotor()
    {
        if(!SceneControl.isZeroVector(motorPosition))
            posModifier += motorPosition*Time.fixedDeltaTime;
        if (!SceneControl.isZeroVector(motorScale))
            scaleModifier += motorScale * Time.fixedDeltaTime;
        if (!SceneControl.isZeroVector(motorRotation))
            rotationModifier += motorRotation * Time.fixedDeltaTime;
        if(!SceneControl.isZeroVector(motorPosition) || !SceneControl.isZeroVector(motorScale) || !SceneControl.isZeroVector(motorRotation))
            updateObject();
    }


    private void updateObject()
    {
        transform.localPosition = objectPosition + posModifier;
        transform.localEulerAngles = objectRotation + rotationModifier;
        transform.localScale = objectScale + scaleModifier;
    }

    public void setValues()
    {
        objectPosition = transform.localPosition;
        objectRotation = transform.localEulerAngles;
        objectScale = transform.localScale;
    }
    public void resetModPosition()
    {
        posModifier = new Vector3(0, 0, 0);
        rotationModifier = new Vector3(0, 0, 0);
        scaleModifier = new Vector3(0, 0, 0);
        motorPosition = new Vector3(0, 0, 0);
        motorScale = new Vector3(0, 0, 0);
        updateObject();
    }
    /**
    private void getObjectValues()
    {
        objectPosition = transform.position - posModifier;
        objectRotation = transform.localEulerAngles - rotationModifier;
        objectScale = transform.localScale - scaleModifier;
    }
    **/
}
