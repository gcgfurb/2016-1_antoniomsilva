﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using System;
using UnityEngine.UI;

public class ModifierManager : MonoBehaviour, ITrackableEventHandler, IVirtualButtonEventHandler{


    public GameObject connector;
    public float buttonCooldown = 1f;
    public Text titleText;
    public Text descriptionText;
    public GameObject[] buttons;
    public GameObject canvasObject;

    private Canvas canvas;
    private float buttonTimer = 0;
    private int activeMod = -1;
    private float modValue = 1f;
    private TrackableBehaviour mTrackableBehaviour;
    private bool activeCanvas = false;
    private LineRenderer line;
    private GameObject connectorSetting = null;
    private float cooldownTimer = 0f;
    private Color colorConnector;

    private float cooldownConnector = 2f;
    private float timerConnector = 0f;

    private SettingManager settingScript = null;

    public int activeModifier { get; private set; }

    // Use this for initialization
    void Start () {
        canvas = canvasObject.GetComponent<Canvas>() as Canvas;
        canvas.enabled = false;
        colorConnector = connector.GetComponent<Renderer>().material.color;

        VirtualButtonBehaviour[] vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
        for (int i = 0; i < vbs.Length; ++i) {
            vbs[i].RegisterEventHandler(this);
        }
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
        line = gameObject.GetComponent<LineRenderer>();
        line.enabled = false;
    }
	
	void Update () {
        activateCanvas();
        setLine();
        UpdateTitle();
        UpdateDesc();
        SceneControl.ButtonCoolDown(ref buttons, ref cooldownTimer, ref buttonCooldown);
        UpdateMotor();
        if (timerConnector >= 0 && timerConnector < cooldownConnector)
            timerConnector += Time.fixedDeltaTime;
    }

    private void checkSettings() {
        if (settingScript.getSelectedObject() == null) {
            connectorSetting = null;
            settingScript = null;
        }
    }

    private void setLine() {
        if (connectorSetting!=null) {
            if (line.enabled && (!settingScript.getActiveCanvas() || !activeCanvas))
                line.enabled = false;
            if(settingScript.getActiveCanvas() && activeCanvas) {
                if (!line.enabled)
                    line.enabled = true;
                line.SetPosition(0, connector.transform.position);
                line.SetPosition(1, connectorSetting.transform.position);
            }
        }
        else if (line!=null) {
            if(line.enabled)
            line.enabled = false;
        }
    }

    private void UpdateTitle() {
        if (!canvas.enabled || settingScript == null)
            return;
        if(settingScript!=null)
            titleText.text = "Modificador: " + modValue + (settingScript.getMotorValue()!=0 ? " (Motor)" : "");
        else
            titleText.text = "Modificador: " + modValue;
    }

    private void UpdateDesc() {
        if (!canvas.enabled || settingScript == null )
            return;
        if (settingScript.getActiveMod() == activeMod)
            return;
        activeMod = settingScript.getActiveMod();
        descriptionText.text = "Valor Atual: " + settingScript.getActualValue();
    }

    private void UpdateMotor() {
        if (settingScript!=null && settingScript.getMotorValue()!=0)
            buttons[3].GetComponent<Renderer>().material.color = Color.green;
    }

    private void updateValue(float value) {
        if (settingScript != null)
            settingScript.setActualValue(settingScript.getModValue() + value);
    }

    private void activateCanvas() {
        if (activeCanvas && settingScript) {
            canvas.enabled = true;
            return;
        }
        if ((!activeCanvas || (settingScript != null && settingScript.getSelectedObject() != null)))
            canvas.enabled = false;
        
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus) {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
            activeCanvas = true;
            if (!line.enabled)
                line.enabled = true;
        }
        else {
            activeCanvas = false;
            if (line != null) {
                if (line.enabled)
                    line.enabled = false;
            }
        }

    }

    public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
    {
        if(canvas.enabled) {
            if (cooldownTimer >= buttonCooldown) {
                switch (vb.VirtualButtonName) {
                    case "Plus":
                        buttonTimer += Time.fixedDeltaTime;
                        buttons[0].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                        cooldownTimer = 0;
                        updateValue(modValue);
                        UpdateTitle();
                        break;
                    case "Minus":
                        buttonTimer += Time.fixedDeltaTime;
                        buttons[1].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                        cooldownTimer = 0;
                        updateValue(-modValue);
                        UpdateTitle();
                        break;
                    case "Change":
                        buttonTimer += Time.fixedDeltaTime;
                        buttons[2].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                        changeValue();
                        cooldownTimer = 0;
                        UpdateTitle();
                        break;
                    case "Motor":
                        buttonTimer += Time.fixedDeltaTime;
                        buttons[3].GetComponent<Renderer>().material.color = new Color(0, 1, 0, 0.5f);
                        if (settingScript.getMotorValue() == 0)
                            settingScript.setMotorValue(modValue);
                        else
                            settingScript.setMotorValue(0);
                        cooldownTimer = 0;
                        UpdateTitle();
                        break;
                }
            }
        }
    }

    private void changeValue()
    {
        if (modValue == 1)
            modValue = 0.1f;
        else if (modValue == 0.1f)
            modValue = 0.01f;
        else if (modValue == 0.01f)
            modValue = 10;
        else
            modValue = 1;
    }

    public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
    {
    }

    void OnTriggerEnter(Collider colliderInfo)
    {
        if (colliderInfo.gameObject.tag == "GuidesConnector") {
            if (timerConnector >= cooldownConnector)
            {
                if (settingScript == null)
                {
                    connectorSetting = colliderInfo.gameObject;
                    connector.GetComponent<Renderer>().material.color = Color.green;
                    connectorSetting.GetComponent<Renderer>().material.color = Color.green;

                    settingScript = connectorSetting.transform.parent.GetComponent<SettingManager>() as SettingManager;
                    UpdateTitle();
                    UpdateDesc();
                }
                else
                {
                    connector.GetComponent<Renderer>().material.color = colorConnector;
                    connectorSetting.GetComponent<Renderer>().material.color = colorConnector;
                    settingScript = null;
                    connectorSetting = null;
                }
                timerConnector = 0;
            }
        }
    }

    
}