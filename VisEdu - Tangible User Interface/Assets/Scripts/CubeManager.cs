﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class CubeManager : MonoBehaviour {

    private GameObject objetoSegurado;
    private LineRenderer line;
    public float distancia = 20;
    private Ray ray;
    private float timer = 0;
    private float secondsToHold = 2;
    private int maskLayer = 1 << 0;

	void Start () {
        line = gameObject.GetComponent<LineRenderer>();
        line.SetColors(Color.red, Color.red);
        line.SetPosition(1, new Vector3(0,distancia,0));
        line.SetWidth(0.5f, 0.5f);
        ray = new Ray(transform.position, Vector3.up);
    }
    void Update() {
        getObjectByLine();
    }
    private void getObjectByLine() {
        ray = new Ray(transform.position, transform.up);
        RaycastHit hitInfo;
        if (objetoSegurado == null) {
            if (Physics.Raycast(ray, out hitInfo, distancia + 2, maskLayer)) {
                if (hitInfo.distance > distancia) {
                    timer = 0;
                    gameObject.GetComponent<Renderer>().material.color = Color.red;
                    line.SetColors(Color.red, Color.red);
                } else {
                    timer += Time.deltaTime;
                    line.SetColors(Color.yellow, Color.yellow);
                    gameObject.GetComponent<Renderer>().material.color = Color.yellow;
                }
                if (timer >= secondsToHold) {
                    line.SetColors(Color.green, Color.green);
                    gameObject.GetComponent<Renderer>().material.color = Color.green;
                    objetoSegurado = hitInfo.collider.gameObject;
                    if (SceneControl.isChildFactoryPlate(objetoSegurado)) {
                        objetoSegurado = Instantiate(objetoSegurado, objetoSegurado.transform.position, Quaternion.identity) as GameObject;
                        objetoSegurado.transform.SetParent(transform);
                        Vector3 scale = objetoSegurado.transform.localScale;
                        objetoSegurado.transform.localScale = scale*25;
                    } else
                        objetoSegurado.transform.SetParent(transform);
                    objetoSegurado.GetComponent<Modifiers>().setValues();
                    objetoSegurado.GetComponent<Modifiers>().resetModPosition();
                    timer = 0;
                }
            } else {
                gameObject.GetComponent<Renderer>().material.color = Color.red;
                line.SetColors(Color.red, Color.red);
            }
        }
        else if (objetoSegurado.transform.parent != transform)
            objetoSegurado = null;
    }
}